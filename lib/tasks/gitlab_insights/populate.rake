namespace :gitlab_insights do
  DEFAULT_API_ENDPOINT = 'https://gitlab.com/api/v4'
  LIMIT = 500

  desc 'Clear resources and populate the DB with data for all projects'
  task :populate, [:token] => :environment do |_, args|
    GitlabInsights::DbPopulator.new(client(args[:token])).execute!
  end

  desc 'Clear resources and populate the DB with data for all projects'
  task :populate_limited, [:token] => :environment do |_, args|
    GitlabInsights::DbPopulator.new(client(args[:token])).execute!(LIMIT)
  end

  def client(token)
    GitlabInsights::Api::Client.new(DEFAULT_API_ENDPOINT, token)
  end
end
