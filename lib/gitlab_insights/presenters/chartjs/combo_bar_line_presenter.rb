# Combo Bar Line Chart expected format
#
# labels: ['January', 'February', 'March'],
# datasets: [{
#   type: 'line',
#   label: 'Dataset 1',
#   borderColor: window.chartColors.blue,
#   borderWidth: 2,
#   data: [0,1,0]
# }, {
#   type: 'bar',
#   label: 'Dataset 2',
#   backgroundColor: window.chartColors.red,
#   data: [2,3,4]
# }
#]

module GitlabInsights
  module Presenters
    module Chartjs
      class ComboBarLinePresenter < Presenter
        def present(data)
          labels = []
          line_data = build_line_dataset
          bar_data = build_bar_dataset

          data.each do |datum|
            labels << datum[:label]
            line_data[:data] << datum[:elements]['line']
            bar_data[:data] << datum[:elements]['bar']
          end

          {
            labels: labels,
            datasets: [
              line_data,
              bar_data
            ]
          }.with_indifferent_access
        end

        private

        def build_line_dataset
          {
            type: 'line',
            label: line_label,
            backgroundColor: nil,
            borderColor: line_color,
            borderWidth: line_width,
            data: []
          }
        end

        def build_bar_dataset
          {
            type: 'bar',
            label: bar_label,
            backgroundColor: bar_color,
            borderColor: bar_color,
            data: []
          }
        end
      end
    end
  end
end
