module GitlabInsights
  module Presenters
    module Chartjs
      class MrsPerMilestonePresenter < ComboBarLinePresenter
        def line_label
          "Rolling Average"
        end

        def line_color
          GitlabInsights::LINE_COLOR
        end

        def line_width
          2
        end

        def bar_label
          "MRs Merged Per Milestone"
        end

        def bar_color
          GitlabInsights::DEFAULT_COLOR
        end
      end
    end
  end
end
