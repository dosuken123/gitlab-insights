module GitlabInsights
  module Presenters
    module Chartjs
      class StackedBarPresenter < Presenter
        def present(results)
          labels = []
          result_datasets = Hash.new { |h, k| h[k] = [] }

          results.each do |result|
            labels << result[:label]

            result[:elements].each do |name, count|
              result_datasets[name] << count
            end
          end

          chart_data_format(labels, result_datasets)
        end

        private

        # labels: ['January', 'February', 'March'],
        # datasets: [
        #   { label: 'x', data: [0,0,1], backgroundColor: 'red' }
        #   { label: 'y', data: [0,1,0], backgroundColor: 'blue' }
        #   { label: 'z', data: [1,0,0], backgroundColor: 'green' }
        # ]

        def dataset_format(label, data, backgroundColor)
          {
            label: label,
            data: data,
            backgroundColor: backgroundColor
          }.with_indifferent_access
        end
      end
    end
  end
end
