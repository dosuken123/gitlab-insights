module GitlabInsights
  module Presenters
    module Chartjs
      class LinePresenter < Presenter
        def present(results)
          labels = []
          result_datasets = Hash.new { |h, k| h[k] = [] }

          results.each do |result|
            labels << result[:label]

            result[:elements].each do |name, count|
              result_datasets[name] << count
            end
          end

          chart_data_format(labels, result_datasets)
        end

        private

        # labels: ['January', 'February', 'March'],
        # datasets: [
        #   {
        #     label: 'x',
        #     data: [0,0,1]
        #   },
        #   {
        #     label: 'y',
        #     data: [0,0,1]
        #   },
        #   {
        #     label: 'z',
        #     data: [0,0,1]
        #   }
        # ]

        def dataset_format(label, data, backgroundColor)
          {
            label: label,
            data: data,
            borderColor: backgroundColor
          }.with_indifferent_access
        end
      end
    end
  end
end
