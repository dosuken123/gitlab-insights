module GitlabInsights
  module Presenters
    module Chartjs
      class BigNumberPercentagePresenter < Presenter
        def present(results)
          chart_data_format.tap do |chart|
            chart[:data] = calculate(results.first[:elements])
            chart[:backgroundColor] = generate_color_code(chart[:data])
          end.with_indifferent_access
        end

        private

        def calculate(results)
          total = results.inject(0) do |total, (name, count)|
            total += count
            total
          end

          uncategorized = results[GitlabInsights::UNCATEGORIZED]

          categorized = total - uncategorized

          (categorized.to_f / total.to_f) * 100.0
        end

        def generate_color_code(total)
          if total > 80
            "#44ad8e"
          elsif total > 70
            "#FDBC60"
          else
            "#E1432A"
          end
        end

        # {
        #   data: 99,
        #   backgroundColor: 'x'
        # }
        def chart_data_format
          Hash.new
        end
      end
    end
  end
end
