module GitlabInsights
  module Presenters
    module Chartjs
      class RegressionBarPresenter < ProgressiveLabelBarPresenter
        def label_name
          "regression"
        end

        def base_color
          "#d9534f"
        end
      end
    end
  end
end
