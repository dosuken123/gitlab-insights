require 'gitlab'

module GitlabInsights
  module Api
    class Client
      attr_reader :client

      def initialize(endpoint, token = nil)
        @client = Gitlab.client(endpoint: endpoint, private_token: token)
      end

      def retrieve_resources(resource_type, path, limit = nil, options = default_options)
        [].tap do |results|
          # HACK because API labels doesn't accept options
          print '.'

          if resource_type != :labels
            first_response = @client.send(resource_type, path, options)
          else
            first_response = @client.send(resource_type, path)
          end

          first_response.auto_paginate do |response|
            print '.'
            results << response
            break if limit && results.length >= limit
          end
        end.map(&:to_hash)
      end

      private

      def default_options
        {
          per_page: 100
        }
      end
    end
  end
end
