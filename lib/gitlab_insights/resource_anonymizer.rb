module GitlabInsights
  class ResourceAnonymizer
    WHITELISTED_ATTRIBUTES = [
      :issues_id,
      :due_date,
      :discussion_locked,
      :time_stats,
      :weight,
      :merge_requests_id,
      :iid,
      :state,
      :created_at,
      :updated_at,
      :closed_at,
      :merged_at,
      :target_branch,
      :source_branch,
      :upvotes,
      :downvotes,
      :labels,
      :work_in_progress,
      :milestone,
      :merge_when_pipeline_succeeds,
      :merge_status,
      :sha,
      :merge_commit_sha,
      :user_notes_count,
      :discussion_locked,
      :should_remove_source_branch,
      :force_remove_source_branch,
      :time_stats,
      :approvals_before_merge,
      :squash,
      :allow_maintainer_to_push,
      :author
    ]

    def initialize(resource)
      @resource = resource
    end

    def anonymize
      @resource.each_key do |field|
        @resource.delete(field) unless WHITELISTED_ATTRIBUTES.include?(field.to_sym)
      end
      @resource
    end
  end
end

