module GitlabInsights
  class DbPopulator
    attr_reader :client

    def initialize(client)
      @client = client
    end

    def execute!(limit = nil)
      Group.all.each do |group|
        group.projects.each do |project|
          project_log(project, 'Retrieving remote project resources', true)

          resources = {
            issues: client.retrieve_resources(:issues, project.full_path, limit),
            merge_requests: client.retrieve_resources(:merge_requests, project.full_path, limit)
          }

          puts
          project_log(project, 'Retrieved remote project resources')
          project_log(project, 'Clearing local project resources', true)

          Issue.delete_all(project: project)
          MergeRequest.delete_all(project: project)

          project_log(project, 'Cleared local project resources')

          add_resources_to_project(project, resources)
          project.revisions.create
        end
      end
    end

    private

    def add_resources_to_project(project, resource_map)
      resource_map.each do |resource_type, resources|
        project_log(project, "Inserting local project #{resource_type}", true)
        resources.each do |resource|
          resource = resource.with_indifferent_access
          resource.delete(:project_id)
          id = resource.delete(:id)
          id_key = "#{resource_type}_id"
          resource = ResourceAnonymizer.new(resource).anonymize
          project.send(resource_type).create(id_key => id).update(resource)
          print '.'
        end
        puts
        project_log(project, "Inserted local project #{resource_type}", true)
      end
    end

    def project_log(project, message, ongoing = false)
      log = "[[#{Time.now.to_s}]]".tap do |msg|
        msg << ' ' << message
        msg << " for #{project.path}"
        msg << '...' if ongoing
      end
      puts log
    end
  end
end
