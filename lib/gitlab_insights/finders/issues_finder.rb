module GitlabInsights
  module Finders
    class IssuesFinder
      attr_reader :project

      def initialize(project)
        @project = project
      end

      def find(opts)
        results = project.issues
        results = results.where(state: opts[:state]) if opts[:state]
        results = results.where("labels @> ARRAY[?]::varchar[]", opts[:labels]) if opts[:labels]
        results = find_dated_before(:created_at, results, opts[:created_before])
        results = find_dated_after(:created_at, results, opts[:created_after])
        results = find_dated_before(:updated_at, results, opts[:updated_before])
        results = find_dated_after(:updated_at, results, opts[:updated_after])
        results = find_dated_before(:closed_at, results, opts[:closed_before])
        results = find_dated_after(:closed_at, results, opts[:closed_after])
        results
      end

      private

      def find_dated_before(field, results, date)
        return results unless date

        results.where("#{field.to_s} < :date", date: date.beginning_of_day)
      end

      def find_dated_after(field, results, date)
        return results unless date

        results.where("#{field.to_s} > :date", date: date.end_of_day)
      end
    end
  end
end
