module GitlabInsights
  module Finders
    class IssuablesPerLabelFinder < IssuablesPerFieldFinder
      include CreatedState

      protected

      def fields
        [field, :labels]
      end

      def format_results(results, opts)
        [
          {
            label: 'bar',
            elements: issuables_with_label_count(results, opts[:collection_labels])
          }
        ]
      end

      def issuables_with_label_count(issuables, labels)
        label_map = labels.each_with_object({}) do |label, hash|
          label_issuables = issuables.select { |i| i[:labels].include?(label) }
          issuables -= label_issuables
          hash[label] = label_issuables.size
        end

        # those that do not fall into the collection labels should also be displayed
        label_map[GitlabInsights::UNCATEGORIZED] = issuables.size
        label_map
      end
    end
  end
end
