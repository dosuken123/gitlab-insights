module GitlabInsights
  module Finders
    module PerDay
      def period_normalizer
        :beginning_of_day
      end

      def period_format
        "%d %b %y"
      end
    end
  end
end
