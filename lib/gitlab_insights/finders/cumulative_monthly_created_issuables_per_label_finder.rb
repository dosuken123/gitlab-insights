module GitlabInsights
  module Finders
    class CumulativeMonthlyCreatedIssuablesPerLabelFinder < MonthlyCreatedIssuablesPerLabelFinder
      def find(opts)
        results = super(opts)

        counts = Hash.new(0)

        results.each do |result|
          result[:elements].each do |name, count|
            counts[name] += count
            result[:elements][name] = counts[name]
          end
        end
      end
    end
  end
end
