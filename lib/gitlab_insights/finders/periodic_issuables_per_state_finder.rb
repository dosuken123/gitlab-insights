module GitlabInsights
  module Finders
    class PeriodicIssuablesPerStateFinder < IssuablesPerStateFinder
      include LimitPeriod
      include PeriodHelper

      protected

      def group_results(results)
        [
          results_for_attribute_by_period(results, :created_at),
          results_for_attribute_by_period(results, :closed_at)
        ]
      end

      def format_results(results, opts)
        formatted_results = period_map(results)
        limit_results(formatted_results, opts[:period_limit])
      end

      def period_map(results_by_period)
        issuables_by_created_period, issuables_by_closed_period = *results_by_period
        period_keys = (issuables_by_created_period.keys + issuables_by_closed_period.keys).uniq.compact.sort
        period_keys.map do |period|
          hash_for_period_count(period, issuables_by_created_period, issuables_by_closed_period)
        end
      end

      def hash_for_period_count(period, created_issuables, closed_issuables)
        {
          label: period.strftime("%B %Y"),
          elements: {
            'Opened' => (created_issuables[period] ? created_issuables[period].count : 0),
            'Closed' => (closed_issuables[period] ? closed_issuables[period].count : 0)
          }
        }.with_indifferent_access
      end
    end
  end
end
