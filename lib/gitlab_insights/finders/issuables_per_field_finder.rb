module GitlabInsights
  module Finders
    class IssuablesPerFieldFinder < BaseIssuableFinder
      def find(opts)
        results = super(opts)
        results = custom_filter_results(results, opts)
        results = pick_fields(results)
        results = group_results(results)
        @results = format_results(results, opts)
      end

      protected

      def custom_filter_results(results, opts)
        results
      end

      def group_results(results)
        results
      end

      private

      def pick_fields(results)
        results.pluck(*fields).map do |result|
          OpenStruct.new(fields.zip(result).to_h)
        end
      end
    end
  end
end
