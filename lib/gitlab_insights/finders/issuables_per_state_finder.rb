module GitlabInsights
  module Finders
    class IssuablesPerStateFinder < IssuablesPerFieldFinder
      include CreatedState

      protected

      def fields
        [:state, :created_at, :closed_at]
      end
    end
  end
end
