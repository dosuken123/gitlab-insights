module GitlabInsights
  module Finders
    class MonthlyIssuablesPerStateFinder < PeriodicIssuablesPerStateFinder
      include PerMonth
    end
  end
end
