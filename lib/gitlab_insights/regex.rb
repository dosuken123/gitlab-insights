module GitlabInsights
  module Regex
    MILESTONE_MATCHER = /\d+\.{1,3}\d?.+/
  end
end
