require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'issuables_per_label_percentage field' do
    let(:view_string) { 'issuables_per_label_percentage' }

    context 'issues' do
      include_examples 'issue query' do
        include_examples 'issuables_per_label_percentage tests'
      end
    end

    context 'merge requests' do
      include_examples 'mr query' do
        include_examples 'issuables_per_label_percentage tests'
      end
    end
  end
end
