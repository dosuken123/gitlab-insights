require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'issuables_per_label field' do
    let(:view_string) { 'issuables_per_label' }

    it_behaves_like 'issuable_per_label view'
  end
end
