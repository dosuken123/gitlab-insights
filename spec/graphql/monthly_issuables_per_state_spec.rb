require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'monthly_issuables_per_state field' do
    let(:view_string) { 'monthly_issuables_per_state' }
    let(:issuable_timestamp_field) { :created_at }

    context 'issuables created and closed' do
      let(:issuable_status) { :open }
      let(:issuable_timestamp_field) { :created_at }

      include_examples 'per month query'

      context 'mrs' do
        include_examples 'mr query' do
          include_examples 'monthly issuables per state tests'
        end
      end

      context 'issues' do
        include_examples 'issue query' do
          include_examples 'monthly issuables per state tests'
        end
      end
    end
  end
end
