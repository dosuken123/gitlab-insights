require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'cumulative_issuables_created_per_month field' do
    let(:view_string) { 'cumulative_issuables_created_per_month' }

    context 'issuables created' do
      let(:issuable_status) { :open }
      let(:issuable_timestamp_field) { :created_at }

      context 'per month' do
        include_examples 'per month query'

        context 'mrs' do
          include_examples 'mr query' do
            include_examples 'cumulative issuables per status per period tests'
          end
        end

        context 'issues' do
          include_examples 'issue query' do
            include_examples 'cumulative issuables per status per period tests'
          end
        end
      end
    end
  end
end
