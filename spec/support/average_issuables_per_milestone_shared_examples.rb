shared_examples_for 'average issuables per milestone' do
  include_examples 'graphql project setup'

  context 'authored by all' do
    let(:exclude_community_contributions) { 'false' }

    context 'project-level' do
      let(:query_string) do
        <<-QUERY
          {
            project(group_path: "#{group_path}", project_path: "#{project1.path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                datasets {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end

      let(:expected) do
        {
          "data": {
            "project": {
              view_string => {
                "labels":[milestone1[:title],milestone2[:title]],
                "datasets": [
                  {
                    type: 'line',
                    label: 'Rolling Average',
                    data: [nil,nil]
                  },
                  {
                    type: 'bar',
                    label: 'MRs Merged Per Milestone',
                    data: [1.0,1.0]
                  }
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end

    context 'group-level' do
      let(:query_string) do
        <<-QUERY
          {
            group(group_path: "#{group_path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                datasets {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "group": {
              view_string => {
                "labels":[milestone1[:title],milestone2[:title]],
                "datasets": [
                  {
                    type: 'line',
                    label: 'Rolling Average',
                    data: [nil,nil]
                  },
                  {
                    type: 'bar',
                    label: 'MRs Merged Per Milestone',
                    data: [1.0,1.0]
                  }
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end
  end

  context 'exclude_community_contributions' do
    let(:exclude_community_contributions) { 'true' }

    context 'project-level' do
      let(:query_string) do
        <<-QUERY
          {
            project(group_path: "#{group_path}", project_path: "#{project1.path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                datasets {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end

      let(:expected) do
        {
          "data": {
            "project": {
              view_string => {
                "labels":[milestone1[:title]],
                "datasets": [
                  {
                    type: 'line',
                    label: 'Rolling Average',
                    data: [nil]
                  },
                  {
                    type: 'bar',
                    label: 'MRs Merged Per Milestone',
                    data: [1.0]
                  }
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end

    context 'group-level' do
      let(:query_string) do
        <<-QUERY
          {
            group(group_path: "#{group_path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                datasets {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "group": {
              view_string => {
                "labels":[milestone1[:title],milestone2[:title]],
                "datasets": [
                  {
                    type: 'line',
                    label: 'Rolling Average',
                    data: [nil,nil]
                  },
                  {
                    type: 'bar',
                    label: 'MRs Merged Per Milestone',
                    data: [1.0,1.0]
                  }
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end
  end
end
