class JsonResponses
  class << self
    def issues
      <<-JSON
        [
          {
            "id": 10237678,
            "iid": 45208,
            "project_id": 13083,
            "title": "HookMergeRequest Error",
            "description": "Version 10.6.3 CE.\n\nWhen my hooks are fired gitlab returns error 500 even when I do the hook test.\n\nWhen I go back to version 10.5.4 the problem no longer occurs.\n\n![Sem_título](/uploads/3d5bdc3e9da907d66f6cec7b7fe986e0/Sem_título.png)",
            "state": "opened",
            "created_at": "2018-04-10T11:43:41.386Z",
            "updated_at": "2018-04-10T11:43:41.386Z",
            "closed_at": null,
            "labels": [],
            "milestone": null,
            "assignees": [],
            "author": {
              "id": 261413,
              "name": "Lucas Krahl",
              "username": "krahl",
              "state": "active",
              "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/261413/avatar.png",
              "web_url": "https://gitlab.com/krahl"
            },
            "assignee": null,
            "user_notes_count": 0,
            "upvotes": 0,
            "downvotes": 0,
            "due_date": null,
            "confidential": false,
            "discussion_locked": null,
            "web_url": "https://gitlab.com/gitlab-org/gitlab-ce/issues/45208",
            "time_stats": {
              "time_estimate": 0,
              "total_time_spent": 0,
              "human_time_estimate": null,
              "human_total_time_spent": null
            },
            "weight": null
          },
          {
            "id": 10237512,
            "iid": 45207,
            "project_id": 13083,
            "title": "JIRA: Issue solving commit messages should include the same content as commit messages which only mentions issues",
            "description": "### Description\n\nWhen mentioning a JIRA issue in a commit, a bunch of useful information is added to the JIRA comment and link:\n![firefox_2018-04-10_13-30-03](/uploads/0f4d25927a793a3ef17f90a706c26729/firefox_2018-04-10_13-30-03.png)\n\nHowever when solving the JIRA issue only the commit hash is added to the message which does not mean much in itself:\n![firefox_2018-04-10_13-29-19](/uploads/9534047dee8b534bda884d5ec19a70fb/firefox_2018-04-10_13-29-19.png)\n\nIt should include the same information (Username, GitLab project, commit message) as a mention comment.",
            "state": "opened",
            "created_at": "2018-04-10T11:34:27.720Z",
            "updated_at": "2018-04-10T11:34:27.720Z",
            "closed_at": null,
            "labels": [],
            "milestone": null,
            "assignees": [],
            "author": {
              "id": 258072,
              "name": "Dániel Tar",
              "username": "qcz",
              "state": "active",
              "avatar_url": "https://secure.gravatar.com/avatar/fac40c9da09ecf65187c3f3149808a2e?s=80&d=identicon",
              "web_url": "https://gitlab.com/qcz"
            },
            "assignee": null,
            "user_notes_count": 0,
            "upvotes": 0,
            "downvotes": 0,
            "due_date": null,
            "confidential": false,
            "discussion_locked": null,
            "web_url": "https://gitlab.com/gitlab-org/gitlab-ce/issues/45207",
            "time_stats": {
              "time_estimate": 0,
              "total_time_spent": 0,
              "human_time_estimate": null,
              "human_total_time_spent": null
            },
            "weight": null
          }
        ]
      JSON
    end

    def labels
      <<-JSON
        [
          {
            "id": 3063810,
            "name": "2fa",
            "color": "#428bca",
            "description": "Issues related to Two-Factor Authentication: http://doc.gitlab.com/ee/profile/two_factor_authentication.html Expert: @rspeicher",
            "open_issues_count": 49,
            "closed_issues_count": 46,
            "open_merge_requests_count": 1,
            "priority": null,
            "subscribed": false
          },
          {
            "id": 2183694,
            "name": "AP1",
            "color": "#8E44AD",
            "description": "Availability/Priority Level 1",
            "open_issues_count": 11,
            "closed_issues_count": 33,
            "open_merge_requests_count": 2,
            "priority": null,
            "subscribed": false
          }
        ]
      JSON
    end

    def merge_requests
      <<-JSON
        [
          {
            "id": 8998122,
            "iid": 18281,
            "project_id": 13083,
            "title": "[Rails5] Fix running spinach tests",
            "description": "## What does this MR do?\n\n1. Adds support for `RAILS5=1|true` for the `bin/spinach` command.\n2. Synchronizes used spinach versions both for rails4 and rails5.\n\nFor rails5 it was accidently used spinach 0.10.1 instead of 0.8.10.\nThat brought some problems on running spinach tests.\n\nExample of failure message:\n\n```\nNoMethodError: undefined method `line' for #<Spinach::Scenario:0x000000000c86ba80>\nDid you mean?  lines\n  /builds/gitlab-org/gitlab-ce/features/support/env.rb:52:in `before_scenario_run'\n```\n\nhttps://gitlab.com/gitlab-org/gitlab-ce/-/jobs/62129156 \n\n## Are there points in the code the reviewer needs to double check?\n\nNo. \n\n## Why was this MR needed?\n\nMigration to Rails 5.0.\n\n## Screenshots (if relevant)\n\nNo. \n\n## Does this MR meet the acceptance criteria?\n\n- [ ] [Changelog entry](https://docs.gitlab.com/ee/development/changelog.html) added, if necessary\n- [ ] [Documentation created/updated](https://docs.gitlab.com/ee/development/doc_styleguide.html)\n- [ ] API support added\n- [ ] Tests added for this feature/bug\n- Review\n  - [ ] Has been reviewed by UX\n  - [ ] Has been reviewed by Frontend\n  - [ ] Has been reviewed by Backend\n  - [ ] Has been reviewed by Database\n- [ ] Conform by the [merge request performance guides](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html)\n- [ ] Conform by the [style guides](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CONTRIBUTING.md#style-guides)\n- [ ] [Squashed related commits together](https://git-scm.com/book/en/Git-Tools-Rewriting-History#Squashing-Commits)\n- [ ] Internationalization required/considered\n- [ ] End-to-end tests pass (`package-and-qa` manual pipeline job)\n\n## What are the relevant issue numbers?\n\n#14286 and !12841",
            "state": "opened",
            "created_at": "2018-04-10T11:25:53.532Z",
            "updated_at": "2018-04-10T11:31:21.192Z",
            "target_branch": "master",
            "source_branch": "blackst0ne-rails5-fix-spinach",
            "upvotes": 0,
            "downvotes": 0,
            "author": {
              "id": 86853,
              "name": "blackst0ne",
              "username": "blackst0ne",
              "state": "active",
              "avatar_url": "https://secure.gravatar.com/avatar/1613b1d2412639606af3866da674f0e1?s=80&d=identicon",
              "web_url": "https://gitlab.com/blackst0ne"
            },
            "assignee": {
              "id": 128633,
              "name": "Rémy Coutable",
              "username": "rymai",
              "state": "active",
              "avatar_url": "https://secure.gravatar.com/avatar/263da227929cc0035cb0eba512bcf81a?s=80&d=identicon",
              "web_url": "https://gitlab.com/rymai"
            },
            "source_project_id": 13083,
            "target_project_id": 13083,
            "labels": [
              "Community contribution",
              "Edge",
              "backend",
              "dependency update",
              "rails5",
              "test"
            ],
            "work_in_progress": false,
            "milestone": {
              "id": 445863,
              "iid": 12,
              "group_id": 9970,
              "title": "10.8",
              "description": "",
              "state": "active",
              "created_at": "2018-01-16T17:05:23.570Z",
              "updated_at": "2018-01-16T17:05:23.570Z",
              "due_date": "2018-05-22",
              "start_date": "2018-04-08"
            },
            "merge_when_pipeline_succeeds": true,
            "merge_status": "can_be_merged",
            "sha": "1a455f3d5c2607c81af4f45a971f310d9210c2ba",
            "merge_commit_sha": null,
            "user_notes_count": 2,
            "discussion_locked": null,
            "should_remove_source_branch": true,
            "force_remove_source_branch": false,
            "web_url": "https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18281",
            "time_stats": {
              "time_estimate": 0,
              "total_time_spent": 0,
              "human_time_estimate": null,
              "human_total_time_spent": null
            },
            "approvals_before_merge": null,
            "squash": true
          },
          {
            "id": 8998113,
            "iid": 18280,
            "project_id": 13083,
            "title": "WIP: Resolve \"Add dropdown to Groups link in top bar\"",
            "description": "## What does this MR do?\n\n## Are there points in the code the reviewer needs to double check?\n\n## Why was this MR needed?\n\n## Screenshots (if relevant)\n\n## Does this MR meet the acceptance criteria?\n\n- [ ] [Changelog entry](https://docs.gitlab.com/ee/development/changelog.html) added, if necessary\n- [ ] [Documentation created/updated](https://docs.gitlab.com/ee/development/doc_styleguide.html)\n- [ ] API support added\n- [ ] Tests added for this feature/bug\n- Review\n  - [ ] Has been reviewed by UX\n  - [ ] Has been reviewed by Frontend\n  - [ ] Has been reviewed by Backend\n  - [ ] Has been reviewed by Database\n- [ ] Conform by the [merge request performance guides](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html)\n- [ ] Conform by the [style guides](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CONTRIBUTING.md#style-guides)\n- [ ] [Squashed related commits together](https://git-scm.com/book/en/Git-Tools-Rewriting-History#Squashing-Commits)\n- [ ] Internationalization required/considered\n- [ ] End-to-end tests pass (`package-and-qa` manual pipeline job)\n\n## What are the relevant issue numbers?\n\n\nCloses #36234",
            "state": "opened",
            "created_at": "2018-04-10T11:25:35.423Z",
            "updated_at": "2018-04-10T11:38:46.934Z",
            "target_branch": "master",
            "source_branch": "36234-nav-add-groups-dropdown",
            "upvotes": 0,
            "downvotes": 0,
            "author": {
              "id": 750946,
              "name": "Dennis Tang",
              "username": "dennistang",
              "state": "active",
              "avatar_url": "https://secure.gravatar.com/avatar/82f4c234d7deecb4760072ecd59f184a?s=80&d=identicon",
              "web_url": "https://gitlab.com/dennistang"
            },
            "assignee": {
              "id": 750946,
              "name": "Dennis Tang",
              "username": "dennistang",
              "state": "active",
              "avatar_url": "https://secure.gravatar.com/avatar/82f4c234d7deecb4760072ecd59f184a?s=80&d=identicon",
              "web_url": "https://gitlab.com/dennistang"
            },
            "source_project_id": 13083,
            "target_project_id": 13083,
            "labels": [
              "frontend",
              "navigation"
            ],
            "work_in_progress": true,
            "milestone": {
              "id": 445863,
              "iid": 12,
              "group_id": 9970,
              "title": "10.8",
              "description": "",
              "state": "active",
              "created_at": "2018-01-16T17:05:23.570Z",
              "updated_at": "2018-01-16T17:05:23.570Z",
              "due_date": "2018-05-22",
              "start_date": "2018-04-08"
            },
            "merge_when_pipeline_succeeds": false,
            "merge_status": "can_be_merged",
            "sha": "2c415f6b3404b768820367aa206e44cdfb59acfe",
            "merge_commit_sha": null,
            "user_notes_count": 0,
            "discussion_locked": null,
            "should_remove_source_branch": null,
            "force_remove_source_branch": true,
            "web_url": "https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18280",
            "time_stats": {
              "time_estimate": 0,
              "total_time_spent": 0,
              "human_time_estimate": null,
              "human_total_time_spent": null
            },
            "approvals_before_merge": null,
            "squash": true
          }
        ]
      JSON
    end

    def milestones
      <<-JSON
        [
          {
            "id": 339386,
            "iid": 44,
            "project_id": 13083,
            "title": "10.0",
            "description": "",
            "state": "closed",
            "created_at": "2017-07-03T13:55:50.520Z",
            "updated_at": "2018-02-26T11:26:06.808Z",
            "due_date": "2017-09-22",
            "start_date": "2017-08-08"
          },
          {
            "id": 315839,
            "iid": 41,
            "project_id": 13083,
            "title": "9.5",
            "description": "",
            "state": "active",
            "created_at": "2017-05-17T16:56:47.216Z",
            "updated_at": "2017-05-17T16:56:47.216Z",
            "due_date": "2017-08-22",
            "start_date": "2017-07-08"
          }
        ]
      JSON
    end

    def pipelines
      <<-JSON
        [
          {
            "id": 20256108,
            "sha": "8656a8860c5979942581d55d09c9ba7786fbb505",
            "ref": "fix/gb/fix-pipeline-statuses-illustrations",
            "status": "success"
          },
          {
            "id": 20255774,
            "sha": "2c415f6b3404b768820367aa206e44cdfb59acfe",
            "ref": "36234-nav-add-groups-dropdown",
            "status": "failed"
          }
        ]
      JSON
    end
  end
end
