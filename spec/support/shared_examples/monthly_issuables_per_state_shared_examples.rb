shared_examples_for 'monthly issuables per state tests' do
  include_examples 'graphql project setup'

  context 'project-level' do
    let(:query_string) do
      <<-QUERY
        {
          project(group_path: "#{group_path}", project_path: "#{project1.path}") {
            #{view_string}(issuable_scope: #{issuable_scope}) {
              labels
              datasets {
                label
                data
              }
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "project": {
            view_string => {
              "labels": [
                period_title_string(issuable1),
                period_title_string(issuable2),
                period_title_string(issuable3)
              ],
              "datasets": [
                {
                  label: 'Opened',
                  "data": [1,1,1]
                },
                {
                  "label": 'Closed',
                  "data": [0,0,0]
                }
              ]
            }
          }
        }
      }.to_json
    end

    it_behaves_like 'graphql query endpoint'
  end

  context 'group-level' do
    let(:query_string) do
      <<-QUERY
        {
          group(group_path: "#{group_path}") {
            #{view_string}(issuable_scope: #{issuable_scope}) {
              labels
              datasets {
                label
                data
              }
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "group": {
            view_string => {
              "labels": [
                period_title_string(issuable1),
                period_title_string(issuable2),
                period_title_string(issuable3),
                period_title_string(issuable4)
              ],
              "datasets": [
                {
                  label: 'Opened',
                  "data": [1,1,1,1]
                },
                {
                  "label": 'Closed',
                  "data": [0,0,0,0]
                }
              ]
            }
          }
        }
      }.to_json
    end

    it_behaves_like 'graphql query endpoint'
  end
end
