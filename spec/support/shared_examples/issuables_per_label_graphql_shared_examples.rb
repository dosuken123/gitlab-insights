shared_examples_for 'issuable_per_label view' do
  shared_examples_for 'issuable_per_label tests' do
    include_examples 'graphql project setup' do
      let(:issuable_status) { :open }
      let(:issuable_timestamp_field) { :created_at }
      let(:query_period) { :month }
      let(:query_period_format) { "%B %Y" }
    end

    context 'project-level' do
      let(:query_string) do
        <<-QUERY
          {
            project(group_path: "#{group_path}", project_path: "#{project1.path}") {
              #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
                labels
                datasets {
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "project": {
              view_string => {
                "labels":[collection_label1,collection_label2,"undefined"],
                "datasets": [
                  data: [1.0,1.0,1.0]
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'

      include_examples 'per team' do
        it_behaves_like 'graphql query endpoint'
      end
    end

    context 'group-level' do
      let(:query_string) do
        <<-QUERY
          {
            group(group_path: "#{group_path}") {
              #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
                labels
                datasets {
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "group": {
              view_string => {
                "labels":[collection_label1,collection_label2,"undefined"],
                "datasets": [
                  data: [1.0,2.0,1.0]
                ]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'

      include_examples 'per team' do
        it_behaves_like 'graphql query endpoint'
      end
    end
  end

  context 'issuable_per_label example' do
    context 'issues' do
      include_examples 'issue query' do
        include_examples 'issuable_per_label tests'
      end
    end

    context 'merge requests' do
      include_examples 'mr query' do
        include_examples 'issuable_per_label tests'
      end
    end
  end
end
