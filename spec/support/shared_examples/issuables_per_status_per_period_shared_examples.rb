shared_examples_for 'issuables per status per period' do
  include_examples 'graphql project setup'

  context 'project-level' do
    let(:query_string) do
      <<-QUERY
        {
          project(group_path: "#{group_path}", project_path: "#{project1.path}") {
            #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
              labels
              datasets {
                label
                data
              }
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "project": {
            view_string => {
              "labels": [
                period_title_string(issuable1),
                period_title_string(issuable2),
                period_title_string(issuable3),
              ],
              "datasets": [
                {
                  "label": collection_label1,
                  "data": [1,0,0]
                },
                {
                  "label": collection_label2,
                  "data": [0,1,0]
                },
                {
                  "label": 'undefined',
                  "data": [0,0,1]
                }
              ]
            }
          }
        }
      }.to_json
    end

    it_behaves_like 'graphql query endpoint'

    include_examples 'per team' do
      it_behaves_like 'graphql query endpoint'
    end
  end

  context 'group-level' do
    let(:query_string) do
      <<-QUERY
        {
          group(group_path: "#{group_path}") {
            #{view_string}(#{state_string}, filter_labels:#{filter_labels.to_json}, collection_labels: #{[collection_label1,collection_label2].to_json}, issuable_scope: #{issuable_scope}) {
              labels
              datasets {
                label
                data
              }
            }
          }
        }
      QUERY
    end
    let(:expected) do
      {
        "data": {
          "group": {
            view_string => {
              "labels": [
                period_title_string(issuable1),
                period_title_string(issuable2),
                period_title_string(issuable3),
                period_title_string(issuable4),
              ],
              "datasets": [
                {
                  "label": collection_label1,
                  "data": [1,0,0,0]
                },
                {
                  "label": collection_label2,
                  "data": [0,1,0,1]
                },
                {
                  "label": 'undefined',
                  "data": [0,0,1,0]
                }
              ]
            }
          }
        }
      }.to_json
    end

    include_examples 'graphql query endpoint'

    include_examples 'per team' do
      it_behaves_like 'graphql query endpoint'
    end
  end
end
