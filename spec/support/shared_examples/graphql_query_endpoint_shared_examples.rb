shared_examples_for 'graphql query endpoint' do
  let(:context) { {} }
  let(:variables) { {} }

  subject do
    res = described_class.execute(
      query_string,
      context: context,
      variables: variables
    )
    if res["errors"]
      puts res
    end
    res.to_json
  end

  it 'result as expected' do
    expect(subject).to eq(expected)
  end
end
