shared_examples_for 'mr query' do
  let(:single_scope) { :merge_request }
  let(:association_scope) { :merge_requests }
end

shared_examples_for 'issue query' do
  let(:single_scope) { :issue }
  let(:association_scope) { :issues }
end

shared_examples_for 'per day query' do
  let(:query_period) { :day }
  let(:query_period_format) { "%d %b %y" }
end

shared_examples_for 'per week query' do
  let(:query_period) { :week }
  let(:query_period_format) { "%d %b %y" }
end

shared_examples_for 'per month query' do
  let(:query_period) { :month }
  let(:query_period_format) { "%B %Y" }
end

shared_examples_for 'per team' do
  let(:filter_labels) { ['filter','team'] }
end
