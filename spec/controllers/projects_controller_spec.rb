require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let(:group) { create(:group) }
  let(:project) { create(:project) }

  describe "GET #show" do
    context 'access by path' do
      it "returns http success" do
        get :show, { group_id: group.path, id: project.path }
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "GET chart views" do
    let(:chart_views) do
      [
        :bugs_by_team,
        :bugs_by_severity,
        :feature_proposals_by_team,
        :issues_by_state
      ]
    end

    context 'access by path' do
      it "returns http success" do
        chart_views.each do |chart_view|
          get chart_view, { group_id: group.path, project_id: project.path }
          expect(response).to have_http_status(:success)
        end
      end
    end
  end
end
