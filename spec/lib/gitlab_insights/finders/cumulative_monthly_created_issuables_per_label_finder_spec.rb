require 'rails_helper'

RSpec.describe GitlabInsights::Finders::CumulativeMonthlyCreatedIssuablesPerLabelFinder do
  shared_examples_for "cumulative monthly created issuable finder" do
    let(:project) { create(:project) }

    let!(:issuable1) { create(single_scope, :open, created_at: Date.new(2018,1,1), labels: ['a', 'z'], project: project) }
    let!(:issuable2) { create(single_scope, :open, created_at: Date.new(2018,2,1), labels: ['a', 'y'], project: project) }
    let!(:issuable3) { create(single_scope, :open, created_at: Date.new(2018,3,1), labels: ['a', 'x'], project: project) }
    let!(:issuable4) { create(single_scope, :open, created_at: Date.new(2018,3,1), labels: ['a', 'b'], project: project) }
    let!(:issuable5) { create(single_scope, :open, created_at: Date.new(2018,4,1), labels: ['a', 'z'], project: project) }

    let(:uncategorized) { GitlabInsights::UNCATEGORIZED }

    subject { described_class.new(project, nil, association_scope) }

    it 'has the correct count per label' do
      opts = {
        state: 'opened',
        filter_labels: ['a'],
        collection_labels: ['x','y','z']
      }

      expected = [
        {
          label: 'January 2018',
          elements: {
            'x': 0,
            'y': 0,
            'z': 1,
            uncategorized => 0
          }
        }.with_indifferent_access,
        {
          label: 'February 2018',
          elements: {
            'x': 0,
            'y': 1,
            'z': 1,
            uncategorized => 0
          }
        }.with_indifferent_access,
        {
          label: 'March 2018',
          elements: {
            'x': 1,
            'y': 1,
            'z': 1,
            uncategorized => 1
          }
        }.with_indifferent_access,
        {
          label: 'April 2018',
          elements: {
            'x': 1,
            'y': 1,
            'z': 2,
            uncategorized => 1
          }
        }.with_indifferent_access
      ]

      actual = subject.find(opts)

      expect(actual).to be_a(Array)

      assert_results(expected, actual, :label)
      assert_results(expected, actual, :elements)
    end
  end

  context 'issues' do
    let(:single_scope) { :issue }
    let(:association_scope) { :issues }

    include_examples "cumulative monthly created issuable finder"
  end

  context 'merge requests' do
    let(:single_scope) { :merge_request }
    let(:association_scope) { :merge_requests }

    include_examples "cumulative monthly created issuable finder"
  end
end
