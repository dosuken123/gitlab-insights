require 'rails_helper'
require 'support/period_finder_spec_helper'

RSpec.describe GitlabInsights::Finders::MonthlyCreatedIssuablesPerLabelFinder do
  shared_examples_for "monthly created issuable per label finder" do
    let(:project) { create(:project) }
    let!(:issuable1) { create(single_scope, :open, created_at: Date.new(2018,1,1), labels: ['a', 'z'], project: project) }
    let!(:issuable2) { create(single_scope, :open, created_at: Date.new(2018,2,1), labels: ['a', 'y'], project: project) }
    let!(:issuable3) { create(single_scope, :open, created_at: Date.new(2018,3,1), labels: ['a', 'x'], project: project) }
    let!(:issuable4) { create(single_scope, :open, created_at: Date.new(2018,3,1), labels: ['a', 'b'], project: project) }

    let(:uncategorized) { GitlabInsights::UNCATEGORIZED }

    subject { described_class.new(project, nil, association_scope).find(opts) }

    let(:expected) do
      [
        {
          label: 'January 2018',
          elements: {
            'x': 0,
            'y': 0,
            'z': 1,
            uncategorized => 0
          }.with_indifferent_access
        },
        {
          label: 'February 2018',
          elements: {
            'x': 0,
            'y': 1,
            'z': 0,
            uncategorized => 0
          }.with_indifferent_access
        },
        {
          label: 'March 2018',
          elements: {
            'x': 1,
            'y': 0,
            'z': 0,
            uncategorized => 1
          }.with_indifferent_access
        }
      ]
    end

    let(:opts) do
      {
        state: 'opened',
        filter_labels: ['a'],
        collection_labels: ['x','y','z']
      }
    end

    it 'has the correct count per label' do
      expect(subject).to be_a(Array)

      assert_results(expected, subject, :elements)
    end

    it_behaves_like "periodic finder", :labels
  end

  context 'issues' do
    let(:single_scope) { :issue }
    let(:association_scope) { :issues }

    include_examples "monthly created issuable per label finder"
  end

  context 'merge requests' do
    let(:single_scope) { :merge_request }
    let(:association_scope) { :merge_requests }

    include_examples "monthly created issuable per label finder"
  end
end
