require 'rails_helper'
require 'gitlab_insights/db_populator'

RSpec.describe GitlabInsights::DbPopulator do
  let(:client) { GitlabInsights::Api::Client.new('') }

  let(:group) { create(:group) }
  let!(:project) { create(:project, group: group) }
  let!(:issue) { create(:issue, issues_id: 999, project: project) }
  let!(:original_issue_count) { project.issues.count }

  subject { GitlabInsights::DbPopulator.new(client) }

  before do
    allow(client).to receive(:retrieve_resources).with(:issues, project.full_path, nil).and_return(ApiResponses.issues)
    allow(client).to receive(:retrieve_resources).with(:merge_requests, project.full_path, nil).and_return(ApiResponses.merge_requests)
  end

  context '#execute' do
    it 'calls the api for resources' do
      expect(client).to receive(:retrieve_resources).with(:issues, project.full_path, nil)
      expect(client).to receive(:retrieve_resources).with(:merge_requests, project.full_path, nil)

      subject.execute!
    end

    it 'adds resources to the project' do
      subject.execute!

      expect(project.reload.issues.length).to eq(ApiResponses.issues.length)
      expect(project.reload.merge_requests.length).to eq(ApiResponses.merge_requests.length)
    end

    it 'creates a new revision' do
      original_count = project.revisions.count

      subject.execute!

      expect(project.reload.revisions.count).to eq(original_count + 1)
    end

    it 'removes existing resources' do
      subject.execute!

      expect(project.reload.issues).not_to include(issue)

      expect do
        issue.reload
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
