require 'rails_helper'

RSpec.describe GitlabInsights::Presenters::Chartjs::StackedBarPresenter do
  let(:input) do
    [
      {
        label: 'January',
        elements: {
          'z': 1,
          'y': 0,
          'x': 0
        }
      }.with_indifferent_access,
      {
        label: 'February',
        elements: {
          'z': 0,
          'y': 1,
          'x': 0
        }
      }.with_indifferent_access,
      {
        label: 'March',
        elements: {
          'z': 0,
          'y': 0,
          'x': 1
        }
      }.with_indifferent_access
    ]
  end

  subject { described_class.new }

  it 'should have the correct format' do
    expected = {
      labels: ['January', 'February', 'March'],
      datasets: [
        {
          label: 'x',
          data: [0,0,1],
          backgroundColor: "#9dd4e4"
        }.with_indifferent_access,
        {
          label: 'y',
          data: [0,1,0],
          backgroundColor: "#415290"
        }.with_indifferent_access,
        {
          label: 'z',
          data: [1,0,0],
          backgroundColor: "#fbade9"
        }.with_indifferent_access
      ]
    }

    actual = subject.present(input)

    expect(actual).to be_a(Hash)
    expect(actual[:labels]).to eq(expected[:labels])
    compare_element_array(actual[:datasets], expected[:datasets])
  end

  def compare_element_array(actual, expected)
    expected.each do |expected_dataset|
      label_identifier = expected_dataset[:label]
      actual_dataset = actual.select { |dataset| dataset[:label] == label_identifier }.first
      expect(actual_dataset[:data]).to eq(expected_dataset[:data])
      expect(actual_dataset[:backgroundColor]).to eq(expected_dataset[:backgroundColor])
    end
  end
end
