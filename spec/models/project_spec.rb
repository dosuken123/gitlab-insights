require 'rails_helper'

RSpec.describe Project, type: :model do
  let(:group) { create(:group) }
  let(:attr) do
    { path: 'my-project' }
  end

  subject { group.projects.create(attr) }

  it { is_expected.to validate_uniqueness_of(:path) }

  it { should respond_to(:revisions) }

  context '#full_path' do
    it 'has the correct path' do
      expect(subject.full_path).to eq("#{group.path}/#{subject.path}")
    end
  end

  context '#latest_revision' do
    let!(:first) { subject.revisions.create }
    let!(:second) { subject.revisions.create }

    it 'returns the last revision' do
      expect(subject.latest_revision).to eq(second)
    end
  end
end
