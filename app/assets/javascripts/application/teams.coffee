team_label_query_segment = (label_json) ->
  label_array = if label_json then JSON.parse(label_json) else []
  label_array.push($('#team-label').val())
  JSON.stringify(label_array)
TeamBugsSeverityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)
TeamBugsPriorityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)
TeamBugsSeverityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)
TeamBugsPriorityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)
TeamRegressionsPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.RegressionsQuery(
    'Open',
    team_label_query_segment(@query_strings.Regression),
    @query_strings.RegressionMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)
TeamMissedDeliverablesPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.MissedDeliverablesQuery(
    'Open',
    team_label_query_segment(@query_strings.MissedDeliverable),
    @query_strings.MissedDeliverableMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)
MRsMergedPerWeekChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesMergedPerWeekQuery(
    'Merged',
    team_label_query_segment(null),
    @query_strings.MergeRequestCategories,
    @query_strings.MergeRequestsScope,
    @query_strings.TeamMRWeeks
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('team-bugs-severity-number')
    TeamBugsSeverityNumber(document.getElementById('team-bugs-severity-number'))
  if document.getElementById('team-bugs-priority-number')
    TeamBugsPriorityNumber(document.getElementById('team-bugs-priority-number'))
  if document.getElementById('team-bugs-severity-pie')
    TeamBugsSeverityPie(document.getElementById('team-bugs-severity-pie'))
  if document.getElementById('team-bugs-priority-pie')
    TeamBugsPriorityPie(document.getElementById('team-bugs-priority-pie'))
  if document.getElementById('team-regressions-per-milestone')
    TeamRegressionsPerMilestoneChart(document.getElementById('team-regressions-per-milestone'))
  if document.getElementById('team-missed-deliverables-per-milestone')
    TeamMissedDeliverablesPerMilestoneChart(document.getElementById('team-missed-deliverables-per-milestone'))
  if document.getElementById('team-mrs-per-week')
    MRsMergedPerWeekChart(document.getElementById('team-mrs-per-week'))
