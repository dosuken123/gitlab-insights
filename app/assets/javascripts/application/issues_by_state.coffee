IssuesOpenedClosedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesOpenedClosedPerMonthQuery(
    @query_strings.IssuesScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('issues-opened-closed-month')
    IssuesOpenedClosedPerMonthChart(document.getElementById('issues-opened-closed-month'))
