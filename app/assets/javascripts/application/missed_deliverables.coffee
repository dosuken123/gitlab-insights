MissedDeliverablesPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.MissedDeliverablesQuery(
    'Open',
    @query_strings.MissedDeliverable,
    @query_strings.MissedDeliverableMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('missed-deliverables-per-milestone')
    MissedDeliverablesPerMilestoneChart(document.getElementById('missed-deliverables-per-milestone'))
