CumulativeBugsCreatedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.CumulativeIssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsCreatedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsCreatedPerDayChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerDayQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsClosedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsClosedPerDayChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerDayQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Teams,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('cumulative-bugs-created-per-month')
    CumulativeBugsCreatedPerMonthChart(document.getElementById('cumulative-bugs-created-per-month'))
  if document.getElementById('bugs-created-per-month')
    BugsCreatedPerMonthChart(document.getElementById('bugs-created-per-month'))
  if document.getElementById('bugs-created-per-day')
    BugsCreatedPerDayChart(document.getElementById('bugs-created-per-day'))
  if document.getElementById('bugs-closed-per-month')
    BugsClosedPerMonthChart(document.getElementById('bugs-closed-per-month'))
  if document.getElementById('bugs-closed-per-day')
    BugsClosedPerDayChart(document.getElementById('bugs-closed-per-day'))
