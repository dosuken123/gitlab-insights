scope = (data) ->
  if data['data']['group']
    'group'
  else
    'project'
transform = (data, view_string) ->
  data_scope = scope(data)
  {
    labels: data['data'][data_scope][view_string]['labels'],
    datasets: data['data'][data_scope][view_string]['datasets']
  }

@charts =
  BarChart: (element, query_string, view_string) ->
    config = {
      type: 'bar',
      options: {
        legend: {
          display: false
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    }
    loadChart(element, config, query_string, view_string)
  StackedBarChart: (element, query_string, view_string) ->
    config = {
      type: 'bar',
      options: {
        scales: {
          xAxes: [{
            stacked: true
            }]
          yAxes: [{
            stacked: true
          }]
        },
        tooltips: {
          mode: 'index'
        }
      }
    }
    loadChart(element, config, query_string, view_string)
  LineChart: (element, query_string, view_string) ->
    config = {
      type: 'line',
      options: {
        elements: {
          line: {
            tension: 0
          }
        },
        scales: {
          yAxes: [{
            ticks: {
                beginAtZero:true
            }
          }]
        }
      }
    }
    loadChart(element, config, query_string, view_string)
  PieChart: (element, query_string, view_string) ->
    config = {
      type: 'pie'
    }
    loadChart(element, config, query_string, view_string)
  BigNumberPercentage: (element, query_string, view_string) ->
    $.ajax({
      url: '/graphql',
      method: 'POST',
      dataType: 'json',
      data: { query: query_string },
      success: (data) ->
        data_scope = scope(data)
        data_element = element.querySelector('.total')
        data_element.innerText = data["data"][data_scope]["issuables_per_label_percentage"]["data"] + '%'
        data_element.style.color = data["data"][data_scope]["issuables_per_label_percentage"]["backgroundColor"]
      async: true
    })
  ComboBarLineChart: (element, query_string, view_string) ->
    config = {
      type: 'bar',
      options: {
        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: true
        },
        elements: {
          line: {
            tension: 0
          }
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      }
    }
    loadChart(element, config, query_string, view_string)

loadChart = (element, chart_config, query_string, view_string) ->
  $.ajax({
    url: '/graphql',
    method: 'POST',
    dataType: 'json',
    data: { query: query_string },
    success: (data) ->
      chart_config['data'] = transform(data, view_string)
      context = element.getContext('2d')
      chart = new Chart(context, chart_config)
    async: true
  })
