CumulativeBugsCreatedByPriorityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.CumulativeIssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsCreatedByPriorityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

BugsClosedByPriorityPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    @query_strings.Bugs,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('cumulative-bugs-created-priority-month')
    CumulativeBugsCreatedByPriorityPerMonthChart(document.getElementById('cumulative-bugs-created-priority-month'))
  if document.getElementById('bugs-created-priority-month')
    BugsCreatedByPriorityPerMonthChart(document.getElementById('bugs-created-priority-month'))
  if document.getElementById('bugs-closed-priority-month')
    BugsClosedByPriorityPerMonthChart(document.getElementById('bugs-closed-priority-month'))
