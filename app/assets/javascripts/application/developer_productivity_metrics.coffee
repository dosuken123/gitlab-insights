AverageMRsMergedPerMilestoneTeam = (element) ->
  query = @queries.ScopedQuery(@queries.AverageIssuablesPerMilestone(
    'Merged',
    @query_strings.MergeRequestsScope,
    true
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)
AverageMRsMergedPerMilestoneAll = (element) ->
  query = @queries.ScopedQuery(@queries.AverageIssuablesPerMilestone(
    'Merged',
    @query_strings.MergeRequestsScope,
    false
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('average-mrs-milestone-team')
    AverageMRsMergedPerMilestoneTeam(document.getElementById('average-mrs-milestone-team'))
  if document.getElementById('average-mrs-milestone-all')
    AverageMRsMergedPerMilestoneAll(document.getElementById('average-mrs-milestone-all'))
