Types::MergeRequestStateEnum = GraphQL::EnumType.define do
  name "MergeRequestState"
  description "State of a GitLab Merge Request"

  value("Open", value: "opened")
  value("Closed", value: "closed")
  value("Reopened", value: "reopened")
  value("Merged", value: "merged")
end
