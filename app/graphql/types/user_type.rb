Types::UserType = GraphQL::ObjectType.define do
  name "User"
  description "Represents a user"

  field :id, !types.ID do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
  field :name, !types.String do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
  field :username, !types.String do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
  field :state, !types.String do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
  field :avatar_url, !types.String do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
  field :web_url, !types.String do
    resolve ->(obj, args, ctx) { obj[ctx.key] }
  end
end
