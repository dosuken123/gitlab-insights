require 'digest/md5'

Types::LineChartDatasetType = GraphQL::ObjectType.define do
  name "LineChartDatasetType"

  field :label, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :data, types[!types.Int] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :fill, !types.Boolean do
    resolve ->(obj, arg, ctx) { true }
  end
  field :borderColor, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
