Types::StackedBarChartType = GraphQL::ObjectType.define do
  name "StackedBarChartType"

  field :labels, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :datasets, types[!Types::StackedBarChartDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
