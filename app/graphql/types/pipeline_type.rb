Types::PipelineType = GraphQL::ObjectType.define do
  name "Pipeline"
  description "A project pipeline"

  field :id, !types.ID
  field :sha, !types.String
  field :ref, !types.String
  field :status, !types.String
end
