Types::MilestoneType = GraphQL::ObjectType.define do
  name "Milestone"

  field :id, !types.ID
  field :iid, !types.ID
  field :title, !types.String
  field :description, !types.String
  field :project_id, !types.ID
  field :group_id, type: !types.ID
  field :created_at, !Types::DateTimeType
  field :updated_at, !Types::DateTimeType
  field :state, !types.String
  field :due_date, !Types::DateTimeType
  field :start_date, !Types::DateTimeType
end


