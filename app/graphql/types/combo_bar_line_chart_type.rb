Types::ComboBarLineChartType = GraphQL::ObjectType.define do
  name "ComboBarLineChartType"

  field :labels, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :datasets, types[!Types::ComboBarLineChartDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
