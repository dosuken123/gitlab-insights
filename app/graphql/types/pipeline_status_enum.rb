Types::PipelineStatusEnum = GraphQL::EnumType.define do
  name "PipelineStatus"
  description "Status of a GitLab pipeline"

  value("Pending", value: "pending")
  value("Running", value: "running")
  value("Success", value: "success")
  value("Failed", value: "failed")
end
