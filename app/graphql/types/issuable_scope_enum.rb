Types::IssuableScopeEnum = GraphQL::EnumType.define do
  name "IssuableScope"
  description "The type of GitLab issuable to query"

  value("Issues", value: "issues")
  value("MergeRequests", value: "merge_requests")
end
