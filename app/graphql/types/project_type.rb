Types::ProjectType = GraphQL::ObjectType.define do
  name "Project"
  description "A GitLab project"

  field :path, !types.String
  field :pipelines, types[!Types::PipelineType] do
    argument :status, Types::PipelineStatusEnum, "Current status of Pipeline"
    argument :ref, types.String, "Ref associated with the pipeline"
    resolve ->(obj, args, ctx) {
      res = obj.pipelines
      res = obj.pipelines.where(status: args[:status]) if args[:status]
      res
    }
  end
  field :issues, types[!Types::IssueType] do
    argument :state, Types::IssuableStateEnum, "Current state of Issue"
    argument :labels, types[types.String], "Labels applied to the Issue"
    argument :created_before, Types::DateTimeType, "Issues created before this date"
    argument :created_after, Types::DateTimeType, "Issues created after this date"
    argument :updated_before, Types::DateTimeType, "Issues updated before this date"
    argument :updated_after, Types::DateTimeType, "Issues updated after this date"
    argument :closed_before, Types::DateTimeType, "Issues closed before this date"
    argument :closed_after, Types::DateTimeType, "Issues closed after this date"
    resolve ->(obj, args, ctx) {
      GitlabInsights::Finders::IssuesFinder.new(obj).find(args)
    }
  end
  field :labels, types[!Types::LabelType]
  field :merge_requests, types[!Types::MergeRequestType] do
    argument :state, Types::MergeRequestStateEnum, "Current state of Merge Request"
    resolve ->(obj, args, ctx) {
      results = obj.merge_requests
      results = results.where(state: args[:state]) if args[:state]
      results
    }
  end
  field :milestones, types[!Types::MilestoneType]

  field :issuables_created_per_month, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::MonthlyCreatedIssuablesPerLabelFinder
    )
  field :cumulative_issuables_created_per_month, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::CumulativeMonthlyCreatedIssuablesPerLabelFinder
    )

  field :issuables_created_per_day, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::DailyCreatedIssuablesPerLabelFinder
    )

  field :issuables_closed_per_month, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::MonthlyClosedIssuablesPerLabelFinder
    )

  field :issuables_merged_per_month, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::MonthlyMergedIssuablesPerLabelFinder
    )

  field :issuables_merged_per_week, !Types::StackedBarChartType,
    function: Functions::PeriodicFindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::WeeklyMergedIssuablesPerLabelFinder
    )

  field :issuables_closed_per_day, !Types::StackedBarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::StackedBarPresenter,
      GitlabInsights::Finders::DailyClosedIssuablesPerLabelFinder
    )

  field :monthly_issuables_per_state, !Types::LineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::LinePresenter,
      GitlabInsights::Finders::MonthlyIssuablesPerStateFinder
    )

  field :issuables_per_label, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::BarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :issuables_per_label_percentage, !Types::BigNumberPercentType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::BigNumberPercentagePresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :regressions, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::RegressionBarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :missed_deliverables, !Types::BarChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::MissedDeliverableBarPresenter,
      GitlabInsights::Finders::IssuablesPerLabelFinder
    )

  field :average_issuables_per_milestone, !Types::ComboBarLineChartType,
    function: Functions::FindIssuables.new(
      GitlabInsights::Presenters::Chartjs::MrsPerMilestonePresenter,
      GitlabInsights::Finders::AverageIssuablesPerMilestoneFinder
    )
end
