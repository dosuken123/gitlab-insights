Types::LabelType = GraphQL::ObjectType.define do
  name "Label"
  description "A project Label"

  field :id, !types.ID
  field :name, !types.String
  field :color, !types.String
  field :description, !types.String
  field :open_issues_count, !types.Int
  field :closed_issues_count, !types.Int
  field :open_merge_requests_count, !types.Int
  field :priority, types.Int
  field :subscribed, !types.Boolean
end
