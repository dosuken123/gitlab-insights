class Functions::FindIssuables < GraphQL::Function
  attr_reader :presenter, :finder_type

  def initialize(presenter_type, finder_type)
    @presenter = presenter_type.new
    @finder_type = finder_type
  end

  argument :state, Types::IssuableStateEnum
  argument :filter_labels, types[types.String]
  argument :collection_labels, types[!types.String]
  argument :issuable_scope, Types::IssuableScopeEnum
  argument :exclude_community_contributions, types.Boolean

  def call(obj, args, ctx)
    finder = finder_type.new(obj, @presenter, args[:issuable_scope])
    finder.find(args)
    finder.present
  end
end
