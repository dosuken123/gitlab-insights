class Functions::PeriodicFindIssuables < Functions::FindIssuables
  attr_reader :presenter, :finder_type

  argument :period_limit, types.Int
end
