class Project  < ActiveRecord::Base
  belongs_to :group
  has_many :revisions

  has_many :issues
  has_many :labels
  has_many :merge_requests
  has_many :milestones
  has_many :pipelines

  validates :path, uniqueness: true

  def full_path
    "#{group.path}/#{path}"
  end

  def latest_revision
    revisions.last
  end
end
