class Issue < ActiveRecord::Base
  belongs_to :project

  def age_in_days
    (Date.today - created_at).to_i
  end

  def age_in_months
    age_in_days / 30
  end
end
