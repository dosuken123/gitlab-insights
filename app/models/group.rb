class Group < ActiveRecord::Base
  has_many :projects
  has_many :issues, through: :projects
  has_many :merge_requests, through: :projects

  validates :path, uniqueness: true

  def full_path
    path
  end
end
