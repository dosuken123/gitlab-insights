class TeamsController < ProjectsController
  TEAMS = {
    'ci/cd' => 'CI/CD'
  }

  before_action :find_group
  before_action :find_project

  def show
    @source = @project || @group
    @team = TEAMS[params[:id]] || params[:id].titleize
  end
end
