# GitLab insights

This is a prototype. The goal is to add immediate value to the GitLab Engineering function, while determining if it adds value to customers. If so, we will work with Victor Wu in Product Management to bring this functionality to GitLab the product.

- [Docker config](docs/docker.md)
- [Postgres](docs/database.md)
- [Data Sync](docs/sync.md)
- [Adding/Checking Data](docs/adding_data.md)
