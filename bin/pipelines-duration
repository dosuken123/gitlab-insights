#!/usr/bin/env ruby

require 'optparse'
require 'httparty'

Options = Struct.new(
  :token,
  :start_date,
  :format,
  :force
)

class AnalyzePipelinesDurationOptionParser
  class << self
    def parse(argv)
      options = Options.new

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: #{__FILE__} [options]\n\n"

        opts.on('-t', '--token ACESS_TOKEN', String, 'A valid access token') do |value|
          options.token = value
        end

        opts.on('-s', '--start-date YYYY-MM-DD', String, 'A starting date') do |value|
          options.start_date = value
        end

        opts.on('-r', '--format [json,csv]', String, 'An export format: json (default), csv') do |value|
          options.format = value.to_sym
        end

        opts.on('-f', '--force', 'Retrieve merge requests count even if we already ghave them') do |value|
          options.force = value
        end

        opts.on('-h', '--help', 'Print help message') do
          $stdout.puts opts
          exit
        end
      end
      parser.parse!(argv)

      options
    end
  end
end

class AnalyzePipelinesDuration
  GITLAB_API = 'https://gitlab.com/api/v4'.freeze
  PIPELINES_URL = "#{GITLAB_API}/projects/gitlab-org%2Fgitlab-ce/pipelines".freeze
  REPORT_FILENAME = 'pipelines'.freeze

  attr_reader :token, :from

  def initialize(token, from)
    @token = token
    @from = from
    @pipelines = load_json_report || []

    assert_token!
  end

  def assert_token!
    return if token

    $stderr.puts "Please provide a valid access token with the `-t/--token` option!"
    exit 1
  end

  def load_json_report
    JSON.parse(File.read(report_filepath)) if File.exist?(report_filepath)
  end

  def pipelines(force = false)
    return @pipelines if !force && @pipelines && @pipelines.any? && latest_created_at <= from

    initial_memo = force ? [] : (@pipelines&.dup || [])

    options = {
      headers: { 'PRIVATE-TOKEN': token },
      query: { ref: 'master', scope: 'finished', status: 'success' }
    }

    loop do
      puts "GET #{PIPELINES_URL} (#{options[:query]})"
      response = HTTParty.get(PIPELINES_URL, options)
      pipeline_ids = response.parsed_response.map { |pi| pi['id'] }

      @pipelines = pipeline_ids.each_with_object(initial_memo) do |pipeline_id, memo|
        next if @pipelines.find { |p| p['id'] == pipeline_id }

        puts "GET #{PIPELINES_URL}/#{pipeline_id}"
        pipeline = HTTParty.get("#{PIPELINES_URL}/#{pipeline_id}", headers: options[:headers]).parsed_response

        created_at = pipeline['created_at']
        memo << {
          'id' => pipeline['id'],
          'sha' => pipeline['sha'],
          'created_at' => created_at,
          'duration' => pipeline['duration']
        }
        break memo << { 'id' => 0 } if Time.parse(created_at) < from
      end

      break if @pipelines.last['id'].zero?

      next_page = Array(response.headers['x-next-page']).first.to_i
      break if next_page.zero?

      options[:query].merge!(page: next_page)
    end

    @pipelines.pop if @pipelines.last['id'].zero?

    @pipelines.sort_by! { |pipeline| pipeline['id'] }
  end

  def latest_created_at
    return nil unless @pipelines&.any?

    latest_pipeline = @pipelines.first

    Time.parse(latest_pipeline.fetch('created_at'))
  end

  def to_file(format = :json)
    data =
      case format
      when :json, :csv
        public_send("as_#{format}")
      else
        raise ArgumentError, "Unknown format #{format}"
      end

    File.write(report_filepath(format), data)
  end

  def report_filepath(format = :json)
    filename =
      case format
      when :json, :csv
        "#{REPORT_FILENAME}.#{format}"
      else
        raise ArgumentError, "Unknown format #{format}"
      end

    File.expand_path("../data/#{filename}", __dir__)
  end

  def as_json
    JSON.pretty_generate(pipelines)
  end

  def as_csv
    csv = ["Pipeline ID, Created at, Duration"]
    pipelines.each do |pipeline|
      csv << "#{pipeline['id']}, #{pipeline['created_at']}, #{pipeline['duration']}"
    end
    csv.join("\n")
  end
end

if $0 == __FILE__
  start = Time.now
  options = AnalyzePipelinesDurationOptionParser.parse(ARGV)
  start_date = options.start_date ? Time.parse(options.start_date) : Time.now - 24 * 3600
  analyzer = AnalyzePipelinesDuration.new(options.token, start_date)

  analyzer.pipelines(options.force)

  analyzer.to_file(options.format || :json)

  puts "\nDone in #{Time.now - start} seconds."
end
