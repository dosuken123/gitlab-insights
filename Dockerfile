FROM ruby:2.4-stretch

EXPOSE 3000

USER root
COPY Gemfile* ./
RUN apt-get update && apt-get install -y build-essential nodejs
RUN bundle install --jobs 20 --retry 5 --without development test
RUN useradd --create-home --shell /bin/bash deploy

USER deploy
WORKDIR /home/deploy
RUN mkdir gitlab-insights
WORKDIR gitlab-insights

COPY --chown=deploy . ./

RUN bundle exec rake assets:precompile RAILS_ENV=review

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
