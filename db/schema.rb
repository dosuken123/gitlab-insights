# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181009150551) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "groups", force: :cascade do |t|
    t.string "path"
  end

  create_table "issues", force: :cascade do |t|
    t.integer  "issues_id"
    t.integer  "iid"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "closed_at"
    t.string   "labels",            default: [], array: true
    t.json     "milestone"
    t.integer  "user_notes_count"
    t.integer  "upvotes"
    t.integer  "downvotes"
    t.string   "due_date"
    t.boolean  "confidential"
    t.boolean  "discussion_locked"
    t.json     "time_stats"
    t.integer  "weight"
    t.integer  "project_id"
    t.json     "author"
  end

  create_table "labels", force: :cascade do |t|
    t.integer "labels_id"
    t.string  "name"
    t.string  "color"
    t.string  "description"
    t.integer "open_issues_count"
    t.integer "closed_issues_count"
    t.integer "open_merge_requests_count"
    t.integer "priority"
    t.boolean "subscribed"
    t.integer "project_id"
  end

  create_table "merge_requests", force: :cascade do |t|
    t.integer  "merge_requests_id"
    t.integer  "iid"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "closed_at"
    t.string   "target_branch"
    t.string   "source_branch"
    t.integer  "upvotes"
    t.integer  "downvotes"
    t.string   "labels",                       default: [], array: true
    t.boolean  "work_in_progress"
    t.json     "milestone"
    t.boolean  "merge_when_pipeline_succeeds"
    t.string   "merge_status"
    t.string   "sha"
    t.string   "merge_commit_sha"
    t.integer  "user_notes_count"
    t.boolean  "discussion_locked"
    t.boolean  "should_remove_source_branch"
    t.boolean  "force_remove_source_branch"
    t.json     "time_stats"
    t.integer  "approvals_before_merge"
    t.boolean  "squash"
    t.boolean  "allow_maintainer_to_push"
    t.integer  "project_id"
    t.json     "author"
    t.datetime "merged_at"
  end

  create_table "milestones", force: :cascade do |t|
    t.integer  "milestones_id"
    t.integer  "iid"
    t.string   "title"
    t.string   "description"
    t.integer  "group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "state"
    t.date     "due_date"
    t.date     "start_date"
    t.integer  "project_id"
  end

  create_table "pipelines", force: :cascade do |t|
    t.integer "pipelines_id"
    t.string  "sha"
    t.string  "ref"
    t.string  "status"
    t.integer "project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string  "path"
    t.integer "group_id"
  end

  create_table "revisions", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "project_id"
  end

  add_foreign_key "issues", "projects"
  add_foreign_key "labels", "projects"
  add_foreign_key "merge_requests", "projects"
  add_foreign_key "milestones", "projects"
  add_foreign_key "pipelines", "projects"
  add_foreign_key "projects", "groups"
  add_foreign_key "revisions", "projects"
end
