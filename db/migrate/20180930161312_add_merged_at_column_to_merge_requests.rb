class AddMergedAtColumnToMergeRequests < ActiveRecord::Migration
  def change
    add_column :merge_requests, :merged_at, :datetime
  end
end
