class AddAuthorColumnToIssues < ActiveRecord::Migration
  def change
    add_column :issues, :author, :json
  end
end
