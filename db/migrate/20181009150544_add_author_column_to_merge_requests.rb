class AddAuthorColumnToMergeRequests < ActiveRecord::Migration
  def change
    add_column :merge_requests, :author, :json
  end
end
