class CreatePipelines < ActiveRecord::Migration
  def change
    create_table :pipelines do |t|
      t.integer :pipelines_id
      t.string :sha
      t.string :ref
      t.string :status
    end

    add_reference :pipelines, :project, foreign_key: true
  end
end
