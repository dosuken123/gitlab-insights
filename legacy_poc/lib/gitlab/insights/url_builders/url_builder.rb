module Gitlab
  module Insights
    module UrlBuilders
      class UrlBuilder
        def initialize(options)
          @host_url = options.fetch(:host_url)
          @api_version = options.fetch(:api_version)
          @project_id = options.fetch(:project_id)
          @resource_type = options.fetch(:resource_type)
          @sub_resource_type = options.fetch(:sub_resource_type, nil)
          @resource_id = options.fetch(:resource_id, nil)
          @params = options.fetch(:params, [])
        end

        def build
          base_url.tap do |url|
            url << "/#{@resource_id}" if @resource_id
            url << "/#{@sub_resource_type}" if @sub_resource_type
            url << params_string if @params
          end
        end

        private

        def host_with_api_url
          "#{@host_url}/api/#{@api_version}"
        end

        def base_url
          "#{host_with_api_url}/projects/#{CGI.escape(@project_id.to_s)}/#{@resource_type}"
        end

        def params_string
          "?" << @params.map do |k, v|
            "#{k}=#{v}"
          end.join("&")
        end
      end
    end
  end
end
