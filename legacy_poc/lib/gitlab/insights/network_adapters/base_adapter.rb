require 'httparty'

module Gitlab
  module Insights
    module NetworkAdapters
      class BaseAdapter
        attr_reader :options

        def initialize(options = {})
          @options = options
        end
      end
    end
  end
end
