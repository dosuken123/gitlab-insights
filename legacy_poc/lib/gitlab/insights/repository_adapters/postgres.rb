require 'pg'
require 'active_record'

module Gitlab
  module Insights
    module RepositoryAdapters
      class Postgres
        def establish_connection
          begin
            PG.connect(dbname: 'postgres').exec("CREATE DATABASE gitlab_insights")
          rescue PG::DuplicateDatabase
            # no op
          end

          ActiveRecord::Base.establish_connection(
            adapter: 'postgresql',
            database: 'gitlab_insights'
          )
        end
      end
    end
  end
end
