require 'sqlite3'
require 'active_record'

module Gitlab
  module Insights
    module RepositoryAdapters
      class Sqlite
        def establish_connection
          ActiveRecord::Base.establish_connection(
            adapter: 'sqlite3',
            database: 'gitlab_insights'
          )
        end
      end
    end
  end
end
